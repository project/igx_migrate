<?php

/**
*  @file Administrative pages
*/

/**
*  General administration form
*/
function igx_migrate_admin_presets() {
  $header = array('Name', 'Description', 'Start URL', 'Sync', 'Edit');
  $rows = array();
  $presets = variable_get('igx_migrate_presets', array());
  foreach ($presets as $preset_id => $preset) {
    $rows[] = array(
      $preset['name'],
      $preset['description'],
      $preset['start_url'],
      l('Sync', 'admin/settings/igx_migrate/sync/'. $preset_id),
      l('Edit', 'admin/settings/igx_migrate/preset/'. $preset_id),
    );
  }
  
  return theme('table', $header, $rows);
}


/**
*  Lists all the content types and mapped associations
*/
function igx_migrate_types_list() {
  $header = array('Type', 'IGX Page Type', 'Preview', 'Edit', 'Delete');
  $node_types = node_get_types();
  $igx_types = igx_migrate_get_types();  
  $i = 0;
  foreach ($node_types as $type => $node_type) {
    $rows[$i] = array(
      $type,
      $igx_types[$type]['igx_page'],
      );
    if ($igx_types[$type]['igx_page']) {
      $rows[$i][] = l('Preview', 'admin/settings/igx_migrate/types/'. $type .'/preview');
      $rows[$i][] = l('Edit', 'admin/settings/igx_migrate/types/'. $type);
      $rows[$i][] = l('Delete', 'admin/settings/igx_migrate/types/'. $type .'/delete');
    }
    else {
      $rows[$i][] = '';
      $rows[$i][] = l('Create', 'admin/settings/igx_migrate/types/'. $type);
      $rows[$i][] = '';
    }
    
    $i++;
  }
  return theme('table', $header, $rows);
}

/**
*  Confirmation form for deleting a page type
*/
function igx_migrate_types_delete_form(&$form_state, $type = NULL) {
  $igx_type = igx_migrate_get_types($type);
  
  $form['_type'] = array(
    '#type' => 'hidden',
    '#value' => $type,
  );
  
  return confirm_form($form,
  t('Are you sure you want to remove the Ingeniux page type @type?', array('@type' => $igx_type[$type]['igx_page'])),
  'admin/settings/igx_migrate/types',
  t('This will remove the relationship between the Ingeniux schema <strong>@schema</strong> and the Drupal node type <strong>@type</strong>, including relationships between fields', array('@schema' => $igx_type[$type]['igx_page'], '@type' => $type)),
  t('Delete'),
  t('Cancel')
  );
}

function igx_migrate_types_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_query('DELETE FROM {igx_migrate_types} WHERE type = "%s"', $form_state['values']['_type']);
    variable_del('igx_type_schema_'. $form_state['values']['_type']);
      variable_del('igx_type_schema_url_'. $form_state['values']['_type']);
    drupal_set_message(t('Page type @type deleted', array('@type' => $form_state['values']['_type'])));
  }
  drupal_goto('admin/settings/igx_migrate/types');
}

/**
*  Delete a field from node types
*/
function igx_migrate_admin_delete_type_field($type, $field) {
  db_query('DELETE FROM {igx_migrate_types} WHERE type = "%s" AND field = "%s"',
            $type, 
            $field);
  drupal_set_message('Field '. $field .' deleted');
  drupal_goto('admin/settings/igx_migrate/types/'. $type);
}

/**
*  Form for mapping Ingeniux page types to content types
*/
function igx_migrate_admin_types_form($form_values = NULL, $type = FALSE) {
  
  $node_types = node_get_types();
  $igx_types = igx_migrate_get_types($type);
  $form = array();
  if (!$type) {
    $type = $form_values['type'];
  }
  $form['type'] = array(
    '#type' => 'hidden',
    '#value' => $type,
  );
  if (!$igx_types[$type]['igx_page'] && !$values['sample_page']) {
    $form['sample_page'] = array(
      '#type' => 'textfield',
      '#title' => t('Sample page URL'),
      '#description' => t('Enter the URL to an existing Ingeniux page as an example of this type'),
    );
    $step = 'new';
    $form['submit'] = array('#type' => 'submit', '#value' => 'Continue');
  }
  $header = array(t('Field'), t('XML Node'), t('Attribute'), t('ID'), t('Delete'));
  $rows = array();
  if ($igx_types[$type]['fields']) {
    foreach ($igx_types[$type]['fields'] as $field_name => $field) {
      foreach ($field as $c => $column) {
        $column_name = ($c)
                       ? ': '. $c
                       : '';
        $rows[] = array(
          $field_name . $column_name,
          $column['node_query'],
          $column['attribute'],
          $column['id'],
          l('Delete', 'admin/settings/igx_migrate/types/delete/'. $type .'/'. $field_name),
        );
      }
    }
  }
  $form['fields'] = array(
    '#value' => theme('table', $header, $rows),
  );
  
  if ($igx_types[$type]['igx_page']) {
  //If a type exists, show the mapping
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
  );
  $form['general']['schema'] = array(
    '#value' => $igx_types[$type]['igx_page'],
    '#prefix' => '<div><strong>'. t('Ingeniux Schema:') .' </strong><br/> ',
    '#suffix' => '</div>',
  );
  $form['general']['node_type'] = array(
    '#value' => $type,
    '#prefix' => '<div><strong>'. t('Node Type:') .'</strong><br/>',
    '#suffix' => '</div>',
  );
    
    $form['new'] = array(
      '#type' => 'fieldset',
      '#title' => t('Add new field'),
    );
    
    $form['new']['new_field'] = array(
      '#type' => 'select',
      '#title' => t('Field'),
      '#options' => _igx_migrate_get_field_list($type),
    );
    
    $form['new']['igx_node'] = array(
      '#type' => 'textfield',
      '#title' => t('Ingeniux XML Node'),
      '#description' => t('Enter the XML query to get the data from the page'),
    );

    $form['new']['igx_attribute'] = array(
      '#type' => 'textfield',
      '#title' => t('Ingeniux XML Attribute'),
      '#description' => t('If applicable, enter the attribute of the node to grab. If empty, the node value will be used.'),
    );
    
    $form['new']['igx_id'] = array(
      '#type' => 'checkbox',
      '#title' => t('Only grab nodes where the ID attribute is the same as the page'),
    );
    
    if (module_exists('filefield')) {
      $form['new']['files'] = array(
        '#type' => 'fieldset',
        '#title' => t('Advanced settings'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      
      $form['new']['files']['filefield'] = array(
        '#type' => 'checkbox',
        '#title' => t('Use embedded links to download files'),
        '#description' => t('<strong>For CCK File Fields Only!</strong> Instead of downloading the entire Ingeniux field content, this option downloads all links to documents and stores them as CCK File Fields instead'),
      );
    if (module_exists('imagefield')) {
      $form['new']['files']['imagefield'] = array(
        '#type' => 'checkbox',
        '#title' => t('Parse embedded images'),
        '#description' => t('<strong>For CCK Image Fields Only!</strong> Instead of downloading the entire Ingeniux field content, this option downloads all images and stores them as CCK Image Fields instead.)'),
      );
      }
    }
    
    $form['submit'] = array('#type' => 'submit', '#value' => 'Enter');

  }
  
  
  $form['step'] = array(
    '#type' => 'hidden',
    '#value' => $step,
  );
  
  return $form;
}

/**
*  Form submit callback for the content type mapping form
*/
function igx_migrate_admin_types_form_submit($form, &$form_state) {
  module_load_include('inc', 'igx_migrate');
  if ($form_state['values']['step'] == 'new') {
    $igx = new igxMigratePage($form_state['values']['sample_page']);
    variable_set('igx_type_schema_'. $form_state['values']['type'], $igx->getPageType());
    variable_set('igx_type_schema_url_'. $form_state['values']['type'], $form_state['values']['sample_page']);
  }
  if ($form_state['values']['new_field']) {
    $data = array();
    if ($form_state['values']['filefield']) {
      $data['filefield'] = TRUE;
    }
    if ($form_state['values']['imagefield']) {
      $data['imagefield'] = TRUE;
    }
    $new_field = unserialize($form_state['values']['new_field']);
    db_query('INSERT INTO {igx_migrate_types} (type, field, node_query, attribute, id, field_column, data) VALUES ("%s", "%s", "%s", "%s", %d, "%s", "%s")', 
      $form_state['values']['type'],
      $new_field['field'],
      $form_state['values']['igx_node'],
      $form_state['values']['igx_attribute'],
      $form_state['values']['igx_id'],
      $new_field['column'],
      serialize($data)
    );
    drupal_set_message('Field '. $new_field['field'] .' entered');
  }
}

/**
*  Form for setting up general module settings
*/
function igx_migrate_admin_settings_form() {
  $form = array();
  
  $form['sync'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update presets automatically'),
  );
  $form['sync']['igx_migrate_sync_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable sync for new content'),
    '#description' => t('When checked, the old Ingeniux site\'s new content will be synced with the Drupal site'),
    '#default_value' => variable_get('igx_migrate_sync_enabled', 0),
  ); 
  
  $form['sync']['igx_migrate_sync_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of articles to sync per cron run'),
    '#description' => t('On each cron run, check these many articles for new content'),
    '#default_value' => variable_get('igx_migrate_sync_number', 20),
  );
  
  $form['directories'] = array(
    '#type' => 'fieldset',
    '#title' => t('File Directories'),
    '#description' => t('Setup where Ingeniux Migrate stores resources like files and images'),
  );
  
  $form['directories']['igx_directory_files'] = array(
    '#type' => 'textfield',
    '#title' => t('File directory'),
    '#description' => t('Where under the files directory any Ingeniux files should be stored'),
    '#default_value' => variable_get('igx_directory_files', 'igx_migrate/files'),
  );

  $form['directories']['igx_directory_images'] = array(
    '#type' => 'textfield',
    '#title' => t('Image directory'),
    '#description' => t('Where under the files directory any Ingeniux images should be stored'),
    '#default_value' => variable_get('igx_directory_images', 'igx_migrate/images'),
  );
  
  return system_settings_form($form);
}

/**
*  Form for adding a new preset 
*/
function igx_migrate_admin_add_preset_form($form, $preset = FALSE) {
  $presets = variable_get('igx_migrate_presets', array());
  $form = array();
  if ($presets[$preset]) {
    $form['preset_edit'] = array(
      '#type' => 'hidden',
      '#value' => $preset,
    );
  }
  $form['preset_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Preset name'),
    '#description' => t('The name of this preset, please use only letters and lowercase (_)'),
    '#required' => TRUE,
    '#default_value' => check_plain($preset),
  );
  
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('A short description for administrators'),
    '#default_value' => $presets[$preset]['description']
  );
  
  if (module_exists('domain')) {
    //If the domain module exists, allow assigning these nodes to a domain
    $domains = array();
    foreach (domain_domains() as $domain) {
      $domains[$domain['domain_id']] = $domain['sitename'] .' ('. $domain['subdomain'] .')';
    }
    $form['domain_options'] = array(
      '#type' => 'fieldset',
      '#title' => 'Domain Options',
      '#collapsible' => TRUE,
    );
    
    $form['domain_options']['domain'] = array(
      '#type' => 'select',
      '#title' => t('Add content from this preset to'),
      '#description' => t('For the domain module, assign content from this preset to the selected domain'),
      '#options' => $domains,
      '#default_value' => $presets[$preset]['domain']
    );
    
    $form['domain_options']['domain_site'] = array(
      '#type' => 'checkbox',
      '#title' => t('Publish content to all affiliates'),
      '#default_value' => $presets[$preset]['domain_site'],
    );
  }
  
  $form['start_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Start URL'),
    '#description' => t('The URL to start crawling from'),
    '#required' => TRUE,
    '#default_value' => $presets[$preset]['start_url']
  );
  
  $form['file_directory'] = array(
    '#type' => 'textfield',
    '#title' => t('Directory for Images and Documents'),
    '#description' => t('<strong>Optional:</strong> Put all documents and images under this directory'),
    '#default_value' => $presets[$preset]['file_directory']
  );

  $form['enable_sync'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep content synced'),
    '#description' => t('When checked, new content and changes will be synced from the Ingeniux site to Drupal.'),
    '#default_value' => $presets[$preset]['enable_sync']
  );

  $form['migrate_images'] = array(
    '#type' => 'checkbox',
    '#title' => t('Migrate images'),
    '#description' => t('Create a file record and move all images to Drupal.'),
    '#default_value' => $presets[$preset]['migrate_images']
  );
  
  if (module_exists('upload')) {
    //If upload exists, allo syncing of linked documents as core upload
    $form['migrate_documents'] = array(
      '#type' => 'checkbox',
      '#title' => t('Migrate documents'),
      '#description' => t('Migrates all linked documents and attaches them to Drupal nodes as file uploads.'),
      '#default_value' => $presets[$preset]['migrate_documents']
    );
  }
    
  if (module_exists('book')) {
    //If book exists, then allow syncing navigation
    $form['keep_navigation'] = array(
      '#type' => 'checkbox',
      '#title' => t('Move site navigation to books'),
      '#description' => t('When checked, content will be put into a book using the book module.'),
      '#default_value' => $presets[$preset]['keep_navigation']
    );
  }
    
  $form['domain_restrict'] = array(
    '#type' => 'radios',
    '#title' => t('Restrict pages'),
    '#options' => array(
      'subdomain' => 'Subdomain (sub.example.com)',
      'domain' => 'Domain (example.com)',
    ),
    '#required' => TRUE,
    '#default_value' => $presets[$preset]['domain_restrict']
  );
  
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Preset'));
  
  return $form;
}

/**
*  Form validation for the add preset form
*/
function igx_migrate_admin_add_preset_form_validate(&$form, &$form_state) {
  module_load_include('inc', 'igx_migrate');
  $request = drupal_http_request(check_url($form_state['values']['start_url'])); 

  if ($request->code == '302' && $request->redirect_code == '200') {
    $form_state['values']['start_url'] = $request->redirect_url;
    $request = drupal_http_request(check_url($request->redirect_url));
   
  }
  if ($request->code != '200') {
    form_set_error('start_url', 
       t('The supplied URL is either not an Ingeniux page or was not available (HTTP code') . $request->code .')');
  }
  $presets = variable_get('igx_migrate_presets', array());
  if ($presets[$form_state['values']['preset_name']] && !$form_state['values']['preset_edit']) {
    form_set_error('preset_name', t('The preset name is already taken'));
  }
}

/**
*  Form submit for the add preset form
*/
function igx_migrate_admin_add_preset_form_submit(&$form, $form_state) {
  $presets = variable_get('igx_migrate_presets', array());
  $presets[$form_state['values']['preset_name']] = array(
    'name' => $form_state['values']['preset_name'],
    'description' => $form_state['values']['description'],
    'start_url' => check_url($form_state['values']['start_url']),
    'domain_restrict' => $form_state['values']['domain_restrict'],
    'keep_navigation' => $form_state['values']['keep_navigation'],
    'migrate_images' => $form_state['values']['migrate_images'],
    'file_directory' => $form_state['values']['file_directory'],
    'migrate_documents' => $form_state['values']['migrate_documents'],
    'enable_sync' => $form_state['values']['enable_sync'],
  );
  igx_migrate_check_directories($form_state['values']['file_directory']);
  if (module_exists('domain')) {
    $presets[$form_state['values']['preset_name']]['domain'] = $form_state['values']['domain'];
    $presets[$form_state['values']['preset_name']]['domain_site'] = $form_state['values']['domain_site'];
  }
  variable_set('igx_migrate_presets', $presets);
  drupal_set_message('Preset saved');
  drupal_goto('admin/settings/igx_migrate');
}

/**
*  Gives a page with a preview of how a sample page would look
*/
function igx_migrate_admin_type_preview($type) {
  module_load_include('inc', 'igx_migrate');
  $igx_types = igx_migrate_get_types($type);
  $url = variable_get('igx_type_schema_url_'. $type, NULL);
  $type = $igx_types[$type];
  $node = igx_migrate_build_node($url, FALSE, TRUE);
  foreach ($type['fields'] as $field_name => $field) {
      $output .= '<hr><h4>'. $field_name .'</h4>';
      $output .= _igx_migrate_admin_type_preview_field($node->$field_name);
  }
  return $output;
}

/**
*  Helper function to display all the field name information
*/
function _igx_migrate_admin_type_preview_field($field) {
  if (is_array($field)) {
    ob_start();
    var_dump($field);
    return '<pre>'. ob_get_clean() .'</pre>';
  }
  return $field;
}

/**
*  Helper function to get all the fields avaialble to a node type
*/
function _igx_migrate_get_field_list($type) {
  $existing = igx_migrate_get_types($type);
  $fields = array();
  foreach (igx_migrate_get_fields($type) as $field_name => $field) {
    if (!$field['columns'] && !$existing[$type]['fields'][$field_name]['']) {
      $fields[serialize(array('field' => $field_name))] = $field['name'];
    }
    elseif (is_array($field['columns'])) {
      foreach ($field['columns'] as $column_name => $column) {
        if (!$existing[$type]['fields'][$field_name][$column_name]) {
          $key = serialize(array('field' => $field_name, 'column' => $column_name));
          $fields[$key] = (count($field['columns']) > 1)
                           ? $column['name']
                           : $field['name'];
        }
      }
    }
  }
  return $fields;
}

/**
*  Sync batch page to get all content from a preset
*/
function igx_migrate_admin_sync_preset($preset) {
  module_load_include('inc', 'igx_migrate');
  $presets = variable_get('igx_migrate_presets', array());
  $page = new igxMigratePage($presets[$preset]['start_url']);
  $page->getNavigation(); 
  $batch = array(
    'operations' => array(
      array('igx_migrate_admin_sync_batch_import', array($presets[$preset]['start_url'], $page->navigation, $presets[$preset])),
    ),
    'finished' => 'igx_migrate_admin_sync_preset_finished',
    'title' => t('Sync content from @name', array('@name' => $presets[$preset]['name'])),
    'init_message' => 'Parsing site navigation',
    'progress_message' => t('Processed @current out of @total pages'),
    'error_message' => t('Sync encountered an error'),
    'file' => drupal_get_path('module', 'igx_migrate') .'/igx_migrate.admin.inc',
  );
  batch_set($batch);
  batch_process('admin/settings/igx_migrate');
}

function igx_migrate_admin_sync_batch_import($start_url, $navigation, $preset, &$context) {
  if (!$context['sandbox']['navigation']) {
    $context['sandbox']['preset'] = $preset;
    $context['sandbox']['navigation'] = $navigation;
    $context['sandbox']['start_url'] = $start_url;
    $context['sandbox']['book_weight'] = -15;
    $context['sandbox']['page_weight'] = array();
  }
  $id = ($context['sandbox']['last_id']) 
      ? $context['sandbox']['last_id']
      : 0;
  if ($id > count($context['sandbox']['navigation'])) {
    $context['finished'] = 1;
    return FALSE;
  }    
  else {
    $context['finished'] = $id /count($context['sandbox']['navigation']);
  }
  if ($context['sandbox']['navigation'][$id]['url']) {
  $context['message'] = 'Parsing '. 
            igx_migrate_build_absolute_url($context['sandbox']['start_url'], 
                                           $context['sandbox']['navigation'][$id]['url']);
    
    if ($context['sandbox']['preset']['keep_navigation']) {
      $book = array('module' => 'book');
      if ($context['sandbox']['navigation'][$id]['level'] == 0) {
        //This is a new book
        $book['bid'] = 'new';
        $book['weight'] = $context['sandbox']['book_weight'];
        $context['sandbox']['book_weight']++;
        $context['sandbox']['page_weight']['new'] = -15;
      }
      else {
        $parent = node_load(igx_migrate_find_nid($context['sandbox']['navigation'][$id]['parent']));
        if (!$context['sandbox']['page_weight'][$parent->nid]) {
          $context['sandbox']['page_weight'][$parent->nid] = -15;
        }
        $book['bid'] = $parent->book['bid'];
        $book['plid'] = $parent->book['mlid'];
        $book['weight'] = $context['sandbox']['page_weight'][$parent->nid];
        $context['sandbox']['page_weight'][$parent->nid]++;
      }
      
    }
    else {
      $book = FALSE;
    }
    $options = array();
    if (module_exists('domain') && $context['sandbox']['preset']['domain']) {
      $options['domain'] = array($context['sandbox']['preset']['domain']);
      $options['domain_site'] = $context['sandbox']['preset']['domain_site'];
    }
    
    $url = igx_migrate_build_absolute_url($context['sandbox']['start_url'], 
                                          $context['sandbox']['navigation'][$id]['url']);
    $node = igx_migrate_build_node($url, 
                                 $book,
                                 TRUE,
                                 $context['sandbox']['preset']['file_directory'],
                                 $options);
    
    if ($context['sandbox']['preset']['migrate_images']) {
      igx_migrate_add_resources($node, 'images', $context['sandbox']['preset']['file_directory']);
    }
    if ($context['sandbox']['preset']['migrate_documents']) {
      igx_migrate_add_resources($node, 'documents', $context['sandbox']['preset']['file_directory']);
    }
  }
  $context['sandbox']['last_id'] = $id + 1;
  
}

/**
*  Finished callback for Ingeniux migrate
*/
function igx_migrate_admin_sync_preset_finished($success, $results, $operations) {
  drupal_set_message('Sync completed');
}