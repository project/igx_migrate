<?php

/**
*  @file Field handling callbacks
*/

/**
*  Formatting callback for node body
*/
function igx_migrate_fields_body($results) {
  if (is_array($results)) {
    return $results[0];
  }
  return $results;
}

/**
*  Formatting callback for node created time
*/
function igx_migrate_fields_created($results) {
  if (is_array($results)) {
    return strtotime($results[0]);
  }
  return $results;
}

/**
*  Formatting callback for node title
*/
function igx_migrate_fields_title($results) {
  if (is_array($results)) {
    return $results[0];
  }
  return $results;
}