<?php

/**
*  @file Helper class to parse Ingeniux files and other functions to 
*  parse images and documents.
*/

/**
*  Helper class which parses Ingeniux files
*/
class igxMigratePage {
  
  /**
  *  The URI of the Ingeniux Page
  */
  var $uri;
  
  /**
  *  A locally-created DOMDocument object
  */
  var $dom;

  /**
  *  The unique Page ID from Ingeniux
  */
  var $page_id;
  
  var $navigation;
  
  var $url_link_to;
  
  var $page_types;
  
  function __construct($uri) {
    $this->uri = trim($uri) .'?tfrm=4';
    $this->loadPage();
    $this->page_types = igx_migrate_get_types();
  }
  
  private function loadPage() {
    $this->dom = new DOMDocument;
    $this->dom->loadXML(file_get_contents($this->uri));

  }
  
  function getPageType() {
    foreach ($this->dom->getElementsByTagName('Page') as $page) {
      if ($page->getAttribute('ID') == $this->getPageId()) {
        return $page->getAttribute('Schema');
      }
    }
  }
  
  function getPageId() {
    if (!$this->page_id) {
      $this->page_id = (get_class($this->dom->firstChild) != 'DOMComment')
        ? $this->dom->firstChild->getAttribute('ID')
        : $this->dom->firstChild->nextSibling->getAttribute('ID');
    }
    return $this->page_id;
  }

  
  function getPageField($query, $attribute = FALSE, $id = FALSE) {
    $query = '//'. ltrim($query, '/');
    $xpath = new DOMXPath($this->dom);
    $result = $xpath->query($query);
    
    if (!$result) {
      return NULL;
    }
    $fields = array();
    foreach ($result as $result) {
       $skip = FALSE;
       if ($id && $result->getAttribute('ID') != $this->getPageId()) {
         $skip = TRUE;
       }
       if ($attribute && !$skip) {
         if ($result->hasAttribute($attribute)) {
           $fields[] = $result->getAttribute($attribute);
         }
       }
       elseif (!$skip && $result->nodeValue) {
         $fields[] = $result->nodeValue;
       }
    }
    
    return $fields;
  }
  
  function getNavigation() {
    $data = array();
    
    foreach ($this->dom->getElementsByTagName('Navigation') as $nav) {
      if ($nav->getAttribute('Name') == 'Primary') {
        $this->buildNavigation($this->getPageId(), 0, $nav->childNodes, $data);
      }
    }
    $this->navigation = $data;
  }
  
  function buildNavigation($parent, $level, $children, &$data) {
    foreach ($children as $child) {
        $leaf['level'] = $level;
        $leaf['id'] = $child->getAttribute('ID');
        $leaf['parent'] = $parent;
        $leaf['url'] = $child->getAttribute('URL');
        $data[] = $leaf;
      if ($child->hasChildNodes()) {
        $this->buildNavigation($leaf['id'], $level + 1, $child->childNodes, $data);  
      }
    }
  }
  
}

function _igx_migrate_get_documents($field, $url, &$node, $base_file = '') {
  $dom = new DOMDocument;
  $result = @$dom->loadHTML($field);
  if (!$result) {
    return $field;
  }
  foreach ($dom->getElementsByTagName('a') as $a) {
    $filename_path = explode('/', $a->getAttribute('href'));
    $filename = array_pop($filename_path);
    if (_igx_migrate_is_document($filename)) {
      $file = _igx_migrate_get_file($url, $a->getAttribute('href'), $filename, $base_file, 'files');
        $result = drupal_write_record('files', $file);
        if ($file->fid) {
            $field = str_replace($a->getAttribute('href'), base_path() . $new_filename, $field);
          $node->files[$file->fid] = $file;
        }
    }
  }
  return $field;
}

/**
*  Retrieves all the images from a field and prepares an array for a filefield image CCK field
*/
function _igx_migrate_get_imagefield($text, $url, $base_file = '') {
  $dom = new DOMDocument;
  $result = @$dom->loadHTML($text);
  if (!$result) {
    return NULL;
  }
  $imagefield = array();
  foreach ($dom->getElementsByTagName('img') as $img) {
    $filename_path = explode('/', $img->getAttribute('src'));
    $filename = array_pop($filename_path);
    $new_filename = file_create_filename($filename, 
                       file_create_path(trim($base_file, '/') .'/'.
                       variable_get('igx_directory_images', 'igx_migrate/images')));
    $image_url = igx_migrate_build_absolute_url($url, $img->getAttribute('src'));
    $tmp_name = file_save_data(_igx_migrate_get_document($image_url), $new_filename);
    $filesize = filesize($tmp_name);
    if ($filesize > 0) {
      file_move($tmp_name, $new_filename);
      $file = _igx_migrate_get_file($url, $img->getAttribute('src'), $filename, $base_file, 'images');
      $result = drupal_write_record('files', $file);
      
      if ($file->fid) {
          $imagefield[$file->fid] = array(
          'data' => array('description' => $img->getAttribute('title'),
                  'alt' => $img->getAttribute('alt'),
                  'title' => $img->getAttribute('title')),
          'filename' => $filename,
          'filepath' => $file->filepath,
          'filesize' => $file->filesize,
          'filemime' => file_get_mimetype($filename),
          'status' => 1,
          'list' => 1,
          'fid' => $file->fid,
          'original_address' => $img->getAttribute('src'),
        );
      $replace[] = array('original' => $img->getAttribute('src'), 'new' => base_path() . $new_filename);
      }
    }
  }
  return array('field' => $imagefield, 'replace' => $replace);
}   

/**
*  Retrieves all the files from a field and prepares an array for a filefield CCK field
*/
function _igx_migrate_get_filefield($text, $url, $base_file = '') {
  $dom = new DOMDocument;
  $result = @$dom->loadHTML($text);
  if (!$result) {
    return NULL;
  }
  $filefield = array();
  foreach ($dom->getElementsByTagName('a') as $a) {
    $filename_path = explode('/', $a->getAttribute('href'));
    $filename = array_pop($filename_path);
    if (_igx_migrate_is_document($filename)) {
      $file = _igx_migrate_get_file($url, $a->getAttribute('href'), $filename, $base_file, 'files');
      $result = drupal_write_record('files', $file);
      if ($file->fid) {
          
          $field = str_replace($a->getAttribute('href'), base_path() . $new_filename, $field);
        $filefield[$file->fid] = array(
          'data' => array('description' => $a->nodeValue),
          'filename' => $filename,
          'filepath' => $file->filepath,
          'filesize' => $file->filesize,
          'filemime' => file_get_mimetype($filename),
          'status' => 1,
          'list' => 1,
          'fid' => $file->fid,
        );
       $replace[] = array('original' => $a->getAttribute('href'), 'new' => base_path() . $new_filename);
        }
    }
  }
  return array('field' => $filefield, 'replace' => $replace);
}

function _igx_migrate_get_file($url, $file_url, $filename, $base_file = '', $type = 'images') {
    $new_filename = file_create_filename($filename, 
                       file_create_path(trim($base_file, '/') .'/'.
                       variable_get('igx_directory_'. $type, 'igx_migrate/'. $type)));
  $doc_url = igx_migrate_build_absolute_url($url, $file_url);
  $tmp_name = file_save_data(_igx_migrate_get_document($doc_url), $new_filename);
  $filesize = filesize($tmp_name);
  file_move($tmp_name, $new_filename);
  $file = (object) array();
  $file->new = TRUE;
  $file->list = TRUE;
  $file->uid = $user->uid;
  $file->filename=$filename;
  $file->filepath=$new_filename;
  $file->filesize=$filesize;
  $file->filemime = file_get_mimetype($filename);
  $file->status = FILE_STATUS_PERMANENT;
  $file->description = strip_tags($a->nodeValue);
  $file->timestamp = time();
  return $file;
}

/**
*  Returns whether or not the linked document is an HTML file or a document to download
*/
function _igx_migrate_is_document($filename) {

  $files = array(
      'ai' => 'application/postscript',
      'aif' => 'audio/x-aiff',
      'aifc' => 'audio/x-aiff',
      'aiff' => 'audio/x-aiff',
      'asc' => 'text/plain',
      'atom' => 'application/atom+xml',
      'au' => 'audio/basic',
      'avi' => 'video/x-msvideo',
      'bcpio' => 'application/x-bcpio',
      'bin' => 'application/octet-stream',
      'bmp' => 'image/bmp',
      'cdf' => 'application/x-netcdf',
      'cgm' => 'image/cgm',
      'class' => 'application/octet-stream',
      'cpio' => 'application/x-cpio',
      'cpt' => 'application/mac-compactpro',
      'csh' => 'application/x-csh',
      'css' => 'text/css',
      'dcr' => 'application/x-director',
      'dif' => 'video/x-dv',
      'dir' => 'application/x-director',
      'djv' => 'image/vnd.djvu',
      'djvu' => 'image/vnd.djvu',
      'dll' => 'application/octet-stream',
      'dmg' => 'application/octet-stream',
      'dms' => 'application/octet-stream',
      'doc' => 'application/msword',
      'docx' => 'application/msword',
      'dtd' => 'application/xml-dtd',
      'dv' => 'video/x-dv',
      'dvi' => 'application/x-dvi',
      'dxr' => 'application/x-director',
      'eps' => 'application/postscript',
      'etx' => 'text/x-setext',
      'exe' => 'application/octet-stream',
      'ez' => 'application/andrew-inset',
      'gif' => 'image/gif',
      'gram' => 'application/srgs',
      'grxml' => 'application/srgs+xml',
      'gtar' => 'application/x-gtar',
      'hdf' => 'application/x-hdf',
      'hqx' => 'application/mac-binhex40',
      'ice' => 'x-conference/x-cooltalk',
      'ico' => 'image/x-icon',
      'ics' => 'text/calendar',
      'ief' => 'image/ief',
      'ifb' => 'text/calendar',
      'iges' => 'model/iges',
      'igs' => 'model/iges',
      'jnlp' => 'application/x-java-jnlp-file',
      'jp2' => 'image/jp2',
      'jpe' => 'image/jpeg',
      'jpeg' => 'image/jpeg',
      'jpg' => 'image/jpeg',
      'kar' => 'audio/midi',
      'latex' => 'application/x-latex',
      'lha' => 'application/octet-stream',
      'lzh' => 'application/octet-stream',
      'm3u' => 'audio/x-mpegurl',
      'm4a' => 'audio/mp4a-latm',
      'm4b' => 'audio/mp4a-latm',
      'm4p' => 'audio/mp4a-latm',
      'm4u' => 'video/vnd.mpegurl',
      'm4v' => 'video/x-m4v',
      'mac' => 'image/x-macpaint',
      'man' => 'application/x-troff-man',
      'mathml' => 'application/mathml+xml',
      'me' => 'application/x-troff-me',
      'mesh' => 'model/mesh',
      'mid' => 'audio/midi',
      'midi' => 'audio/midi',
      'mif' => 'application/vnd.mif',
      'mov' => 'video/quicktime',
      'movie' => 'video/x-sgi-movie',
      'mp2' => 'audio/mpeg',
      'mp3' => 'audio/mpeg',
      'mp4' => 'video/mp4',
      'mpe' => 'video/mpeg',
      'mpeg' => 'video/mpeg',
      'mpg' => 'video/mpeg',
      'mpga' => 'audio/mpeg',
      'ms' => 'application/x-troff-ms',
      'msh' => 'model/mesh',
      'mxu' => 'video/vnd.mpegurl',
      'nc' => 'application/x-netcdf',
      'oda' => 'application/oda',
      'ogg' => 'application/ogg',
      'pbm' => 'image/x-portable-bitmap',
      'pct' => 'image/pict',
      'pdb' => 'chemical/x-pdb',
      'pdf' => 'application/pdf',
      'pgm' => 'image/x-portable-graymap',
      'pgn' => 'application/x-chess-pgn',
      'pic' => 'image/pict',
      'pict' => 'image/pict',
      'png' => 'image/png',
      'pnm' => 'image/x-portable-anymap',
      'pnt' => 'image/x-macpaint',
      'pntg' => 'image/x-macpaint',
      'ppm' => 'image/x-portable-pixmap',
      'ppt' => 'application/vnd.ms-powerpoint',
      'ps' => 'application/postscript',
      'qt' => 'video/quicktime',
      'qti' => 'image/x-quicktime',
      'qtif' => 'image/x-quicktime',
      'ra' => 'audio/x-pn-realaudio',
      'ram' => 'audio/x-pn-realaudio',
      'ras' => 'image/x-cmu-raster',
      'rdf' => 'application/rdf+xml',
      'rgb' => 'image/x-rgb',
      'rm' => 'application/vnd.rn-realmedia',
      'roff' => 'application/x-troff',
      'rtf' => 'text/rtf',
      'rtx' => 'text/richtext',
      'sgm' => 'text/sgml',
      'sgml' => 'text/sgml',
      'sh' => 'application/x-sh',
      'shar' => 'application/x-shar',
      'silo' => 'model/mesh',
      'sit' => 'application/x-stuffit',
      'skd' => 'application/x-koan',
      'skm' => 'application/x-koan',
      'skp' => 'application/x-koan',
      'skt' => 'application/x-koan',
      'smi' => 'application/smil',
      'smil' => 'application/smil',
      'snd' => 'audio/basic',
      'so' => 'application/octet-stream',
      'spl' => 'application/x-futuresplash',
      'src' => 'application/x-wais-source',
      'sv4cpio' => 'application/x-sv4cpio',
      'sv4crc' => 'application/x-sv4crc',
      'svg' => 'image/svg+xml',
      'swf' => 'application/x-shockwave-flash',
      't' => 'application/x-troff',
      'tar' => 'application/x-tar',
      'tcl' => 'application/x-tcl',
      'tex' => 'application/x-tex',
      'texi' => 'application/x-texinfo',
      'texinfo' => 'application/x-texinfo',
      'tif' => 'image/tiff',
      'tiff' => 'image/tiff',
      'tr' => 'application/x-troff',
      'tsv' => 'text/tab-separated-values',
      'txt' => 'text/plain',
      'ustar' => 'application/x-ustar',
      'vcd' => 'application/x-cdlink',
      'vrml' => 'model/vrml',
      'vxml' => 'application/voicexml+xml',
      'wav' => 'audio/x-wav',
      'wbmp' => 'image/vnd.wap.wbmp',
      'wbmxl' => 'application/vnd.wap.wbxml',
      'wml' => 'text/vnd.wap.wml',
      'wmlc' => 'application/vnd.wap.wmlc',
      'wmls' => 'text/vnd.wap.wmlscript',
      'wmlsc' => 'application/vnd.wap.wmlscriptc',
      'wrl' => 'model/vrml',
      'xbm' => 'image/x-xbitmap',
      'xht' => 'application/xhtml+xml',
      'xls' => 'application/vnd.ms-excel',
      'xpm' => 'image/x-xpixmap',
      'xsl' => 'application/xml',
      'xslt' => 'application/xslt+xml',
      'xul' => 'application/vnd.mozilla.xul+xml',
      'xwd' => 'image/x-xwindowdump',
      'xyz' => 'chemical/x-xyz',
      'zip' => 'application/zip'
      );
  $extension = substr($filename, strrpos($filename, '.') + 1  , strlen($filename) - strrpos($filename, '.'));
  return $files[$extension];
}

/**
*  Retrieves a document off the internet
*/
function _igx_migrate_get_document($url) {
   $url_stuff = parse_url($url);
   $port = isset($url_stuff['port']) ? $url_stuff['port']:80;

   $fp = fsockopen($url_stuff['host'], $port);

   $query  = 'GET ' . $url_stuff['path'] . " HTTP/1.0\n";
   $query .= 'Host: ' . $url_stuff['host'];
   $query .= "\n\n";

   fwrite($fp, $query);

   while ($line = fread($fp, 1024)) {
       $buffer .= $line;
   }

   preg_match('/Content-Length: ([0-9]+)/', $buffer, $parts);
   return substr($buffer, - $parts[1]);  
}

/**
*  Helper function that searches for images in a field and then creates a field record
*  @param The field text
*  @param The URL to the current page in Ingeniux (to find relative images)
*  @return The field with all image sources changed to local Drupal files
*/
function _igx_migrate_get_images($field, $url, &$node, $base_file = '') {

  $dom = new DOMDocument;
  $result = @$dom->loadHTML($field);
  if (!$result) {
    return NULL;
  }
  foreach ($dom->getElementsByTagName('img') as $img) {
    $filename_path = explode('/', $img->getAttribute('src'));
    $filename = array_pop($filename_path);
    $new_filename = file_create_filename($filename, 
                       file_create_path(trim($base_file, '/') .'/'.
                       variable_get('igx_directory_images', 'igx_migrate/images')));
    $image_url = igx_migrate_build_absolute_url($url, $img->getAttribute('src'));
    $tmp_name = file_save_data(_igx_migrate_get_document($image_url), $new_filename);
    $filesize = filesize($tmp_name);
    if ($filesize > 0) {
      file_move($tmp_name, $new_filename);
      $file = (object) array();
      $file->uid = $user->uid;
      $file->filename=$filename;
      $file->filepath=$new_filename;
      $file->status = FILE_STATUS_PERMANENT;
      $file->filesize = $filesize;
      $file->filemime = file_get_mimetype($filename);
      $file->timestamp = time();
      $result = drupal_write_record('files', $file);
      
      if ($file->fid) {
          $field = str_replace($img->getAttribute('src'), base_path() . $new_filename, $field);
      }
    }
  }
  return $field;
}