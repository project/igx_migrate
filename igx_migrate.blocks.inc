<?php

/**
*  @file Blocks
*/

function igx_migrate_block_node_info() {
  if ($node = menu_get_object()) {
    if ($node->igx_migrate) {
      return array('subject' => 'Ingeniux Info',
               'content' => '<strong>Original URL: </strong>'. 
                   l($node->igx_migrate['url'], $node->igx_migrate['url']));
    }
  }
}

function igx_migrate_block_manually_migrate_form() {
  $form = array();
  
  $form['igx_url'] = array(
    '#type' => 'textfield',
    '#title' => 'URL to page',
    '#required' => TRUE,
    '#description' => t('Enter the URL to an Ingeniux page to migrate'),
    '#attributes' => array('style' => 'width: 100%;'),
  );
  
  $form['submit'] = array('#type' => 'submit', '#value' => 'Migrate');
  
  return $form;
}

function igx_migrate_block_manually_migrate_form_validate($form, &$form_state) {
  $url = parse_url($form_state['values']['igx_url']);
  if (!$url['scheme']) {
    form_set_error('igx_url', 'Enter a fully-qualified URL');
  }
  elseif (!drupal_http_request($form_state['values']['igx_url'])) {
    form_set_error('igx_url', 'The URL is not available');
  }
}

function igx_migrate_block_manually_migrate_form_submit($form, &$form_state) {
  $node = igx_migrate_build_node($form_state['values']['igx_url']);
  drupal_set_message('Page migrated from Ingeniux');
  drupal_goto('node/'. $node->nid);
}